import argparse
import awkward
import os.path as osp
import os
import glob
import torch
import awkward as ak
import time
import uproot
import uproot3
import yaml
import numpy as np
import torch.nn.functional as F
import torch.nn as nn
from torch_geometric.datasets import MNISTSuperpixels
from torch_geometric.data import DataListLoader, DataLoader
import torch_geometric.transforms as T
from torch_geometric.nn import SplineConv, global_mean_pool, DataParallel, EdgeConv, GATConv,PNAConv
from torch_geometric.data import Data
import scipy.sparse as ss
from datetime import datetime, timedelta
from torch_geometric.utils import degree
import os.path


from tools.GNN_model_weight.models import *
from tools.GNN_model_weight.utils  import *


print("Libraries loaded!")


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Train with configurations')
    add_arg = parser.add_argument
    add_arg('config', help="job configuration")
    args = parser.parse_args()
    config_file = args.config
    config = load_yaml(config_file)

    path_to_test_file = config['data']['path_to_test_file']
    files = glob.glob(path_to_test_file)

    print ("files:",files)
    #intreename = "FlatSubstructureJetTree"
    intreename = "outtree"
    path_to_outdir = config['data']['path_to_outdir']


    path_to_combined_ckpt = config['test']['path_to_combined_ckpt']

    output_name = config['test']['output_name']

    nentries_total = 0
    nentries_done = 0

    for file in files:
        nentries_total += uproot3.numentries(file, intreename)

    print("Evaluating on {} files with {} entries in total.".format(len(files), nentries_total))


    #Load tf keras model
    # jet_type = "Akt10RecoChargedJet" #track jets
    jet_type = "Akt10TruthJet" #UFO jets
    #print(files)

    t_filestart = time.time()

    for file in files:

        t_start = time.time()

        dsids = np.array([])
        dr_w_boson = np.array([])
        dr_z_boson = np.array([])
        NBHadrons = np.array([])
        mcweights = np.array([])
        ptweights = np.array([])

        all_lund_zs = np.array([])
        all_lund_kts = np.array([])
        all_lund_drs = np.array([])

        parent1 = np.array([])
        parent2 = np.array([])

        jet_pts = np.array([])
        jet_phis = np.array([])
        jet_etas = np.array([])
        jet_ms = np.array([])
        eta = np.array([])
        jet_truth_pts = np.array([])
        jet_truth_etas = np.array([])
        jet_truth_dRmatched = np.array([])
        jet_truth_split = np.array([])
        jet_ungroomed_ms = np.array([])
        jet_ungroomed_pts = np.array([])
        vector = []

        print("Loading file",file)

        #with uproot.open(file) as infile:
        with uproot.open(file) as infile:

            tree = infile[intreename]
            max_entry = min(tree.num_entries, 10000000)
            dsids = np.append( dsids, np.array(tree["DSID"].array()) )
            dr_w_boson = np.append( dr_w_boson, ak.to_numpy(ak.firsts(tree["Akt10TruthJet_jetDrWboson"].array(entry_stop=max_entry))) ) 
            dr_z_boson = np.append( dr_z_boson, ak.to_numpy(ak.firsts(tree["Akt10TruthJet_jetDrZboson"].array(entry_stop=max_entry))) )
            #eta = ak.concatenate(eta, pad_ak3(tree["Akt10TruthJet_jetEta"].array(), 30),axis=0)
            mcweights = np.append( mcweights, np.array(tree["mcWeight"].array()) )
#            ptweights = np.append( ptweights, np.array(tree["fjet_testing_weight_pt"].array()) )
#            NBHadrons = np.append( NBHadrons, ak.to_numpy(tree["Akt10UFOJet_GhostBHadronsFinalCount"].array()))

#            parent1 = np.append(parent1, tree["UFO_edge1"].array(library="np"),axis=0)
#            parent2 = np.append(parent2, tree["UFO_edge2"].array(library="np"),axis=0)
#
#            #Get jet kinematics
#            jet_pts = np.append(jet_pts, tree["UFOSD_jetPt"].array(library="np"))
#            jet_etas = np.append(jet_etas, tree["UFOSD_jetEta"].array(library="np"))
#            jet_phis = np.append(jet_phis, tree["UFOSD_jetPhi"].array(library="np"))
#            jet_ms = np.append(jet_ms, tree["UFOSD_jetM"].array(library="np"))
#
#    #        jet_truth_pts = np.append(jet_truth_pts, tree["Truth_jetPt"].array(library="np"))
#    #        jet_truth_etas = np.append(jet_truth_etas, ak.to_numpy(tree["Truth_jetEta"].array(library="np")))
#
#
#            #Get Lund variables
#            all_lund_zs = np.append(all_lund_zs,tree["UFO_jetLundz"].array(library="np") )
#            all_lund_kts = np.append(all_lund_kts, tree["UFO_jetLundKt"].array(library="np") )
#            all_lund_drs = np.append(all_lund_drs, tree["UFO_jetLundDeltaR"].array(library="np") )

            # - - - Here is how to get the branches from the old ntuples and append them to the new files.
            NBHadrons = np.append( NBHadrons, ak.to_numpy(ak.firsts(tree["Akt10TruthJet_GhostBHadronsFinalCount"].array(entry_stop=max_entry))), axis=0 )

            jet_ms = np.append( jet_ms, ak.to_numpy(ak.firsts(tree["Akt10TruthJet_jetM"].array(entry_stop=max_entry))) )
            jet_pts = np.append( jet_pts, ak.to_numpy(ak.firsts(tree["Akt10TruthJet_jetPt"].array(entry_stop=max_entry))) )
            jet_etas = np.append( jet_etas, ak.to_numpy(ak.firsts(tree["Akt10TruthJet_jetEta"].array(entry_stop=max_entry))) )
            jet_phis = np.append( jet_phis, ak.to_numpy(ak.firsts(tree["Akt10TruthJet_jetPhi"].array(entry_stop=max_entry))) )

            print("Getting ljp branches...")
            parent1 = ak.concatenate( [parent1, ak.firsts(tree["Akt10TruthJet_jetLundIDParent1"].array(entry_stop=max_entry))] )
            parent2 = ak.concatenate( [parent2, ak.firsts(tree["Akt10TruthJet_jetLundIDParent1"].array(entry_stop=max_entry))] )

            all_lund_zs = ak.concatenate( [all_lund_zs, ak.firsts(tree["Akt10TruthJet_jetLundZ"].array(entry_stop=max_entry))] )
            all_lund_kts = ak.concatenate( [all_lund_kts, ak.firsts(tree["Akt10TruthJet_jetLundKt"].array(entry_stop=max_entry))] )
            all_lund_drs = ak.concatenate( [all_lund_drs, ak.firsts(tree["Akt10TruthJet_jetLundDeltaR"].array(entry_stop=max_entry))] )
            print("...done!")



        #Get labels
        #labels = ( dsids == 0 ) & ( jet_ms < 85e3 )
        labels = (( dsids < 1 ) | (dsids > 370000)) & ( dr_w_boson < 0.1 ) & (dr_z_boson > 1.0)
   
        print (int(labels.sum()),"labeled as signal out of", len(labels), "total events")
        
        labels = to_categorical(labels, 2)
        labels = np.reshape(labels[:,1], (len(all_lund_zs), 1))

        #Calculate flat pT weights
        ptweights = np.vectorize(GetPtWeight)(dsids, jet_pts, filename=config['data']['weights_file'], SF=config['data']['scale_factor'])

        #dataset = create_train_dataset_fulld_new(all_lund_zs, all_lund_kts, all_lund_drs, parent1, parent2, labels)
        dataset = create_train_dataset_fulld(all_lund_zs, all_lund_kts, all_lund_drs, parent1, parent2, ptweights, labels)
        s_evt = 0
        events = 100
        #dataset = create_train_dataset_fulld_new(all_lund_zs[s_evt:events], all_lund_kts[s_evt:events], all_lund_drs[s_evt:events], parent1[s_evt:events], parent2[s_evt:events], labels[s_evt:events])

        print("Dataset created!")
        delta_t_fileax = time.time() - t_start
        print("Created dataset in {:.4f} seconds.".format(delta_t_fileax))

        batch_size = config['architecture']['batch_size']

        test_loader = DataLoader(dataset, batch_size=batch_size, shuffle=False)

        print ("dataset dataset size:", len(dataset))


        #EVALUATING
        #torch.save(model.state_dict(), path)
        choose_model = config['architecture']['choose_model']
        learning_rate = config['architecture']['learning_rate']

        if choose_model == "LundNet":
            model = LundNet()
        if choose_model == "GATNet":
            model = GATNet()
        if choose_model == "GINNet":
            model = GINNet()
        if choose_model == "EdgeGinNet":
            model = EdgeGinNet()
        if choose_model == "PNANet":
            model = PNANet()

        model.load_state_dict(torch.load(path_to_combined_ckpt))

        device = torch.device('cuda') # Usually gpu 4 worked best, it had the most memory available
        model.to(device)
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

        #Predict scores
        y_pred = get_scores(test_loader, model, device)
   
        tagger_scores = y_pred[:,0]
        print("Tagger scores",tagger_scores)
        delta_t_pred = time.time() - t_filestart - delta_t_fileax
        print("Calculated predicitions in {:.4f} seconds,".format(delta_t_pred))

        #Save root files containing model scores
        filename = file.split("/")[-1]
        outfile_path = os.path.join(path_to_outdir, filename)

        print ("dsids",len(dsids),"mcweights",len(mcweights),"NBHadrons",len(NBHadrons),"tagger_scores",len(tagger_scores),"jet_pts",len(jet_pts),"jet_etas",len(jet_phis),"jet_phis",len(jet_phis),"jet_ms",len(jet_ms),"ptweights",len(ptweights))
        with uproot3.recreate("{}_score_{}.root".format(outfile_path, output_name)) as f:

            treename = "FlatSubstructureJetTree"

            #Declare branch data types
            f[treename] = uproot3.newtree({"EventInfo_mcChannelNumber": "int32",
                                          "EventInfo_mcEventWeight": "float32",
                                          "EventInfo_NBHadrons": "int32",   # I doubt saving the parents is necessary here
                                          "fjet_nnscore": "float32",        # which is why I didn't include them
                                          "fjet_pt": "float32",
                                          "fjet_eta": "float32",
                                          "fjet_phi": "float32",
                                          "fjet_m": "float32",
                                          "fjet_weight_pt": "float32",
                                          "fdr_w_boson": "float32",
                                          "fdr_z_boson": "float32"
                                          })

            #Save branches
            f[treename].extend({"EventInfo_mcChannelNumber": dsids,
                                "EventInfo_mcEventWeight": mcweights,
                                "EventInfo_NBHadrons": NBHadrons,
                                "fjet_nnscore": tagger_scores,
                                "fjet_pt": jet_pts,
                                "fjet_eta": jet_etas,
                                "fjet_phi": jet_phis,
                                "fjet_m": jet_ms,
                                "fjet_weight_pt": ptweights,
                                "fdr_w_boson": dr_w_boson,
                                "fdr_z_boson": dr_z_boson
                                })

        delta_t_save = time.time() - t_start - delta_t_fileax - delta_t_pred
        print("Saved data in {:.4f} seconds.".format(delta_t_save))

        #nentries = 0
        #Time statistics
        nentries_done += uproot3.numentries(file, intreename)
        time_per_entry = (time.time() - t_start)/nentries_done
        eta = time_per_entry * (nentries_total - nentries_done)

        print("Evaluated on {} out of {} events".format(nentries_done, nentries_total))
        print("Estimated time until completion: {}".format(str(timedelta(seconds=eta))))


    print("Total evaluation time: {:.4f} seconds.".format(time.time()-t_filestart))
